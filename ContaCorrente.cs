using System;

public class ContaCorrente : Conta
{
    private DateTime dataAbertura;
    private decimal limite;

    public ContaCorrente(string t, decimal l)
        : base(t)
    {
        dataAbertura = DateTime.Today;
        limite = l;
    }

    public DateTime DataAbertura
    {
        get { return dataAbertura; }
    }
    public decimal Limite
    {
        get { return limite; }
    }

    public override string Id
    {
        get { return this.Titular + "(CC)"; }
    }

    public override void Sacar(decimal valor)
    {
        if ((saldo + limite) < valor){
            throw new ArgumentOutOfRangeException("Saldo insuficiente.");
        }
        saldo -= valor;
    }

}