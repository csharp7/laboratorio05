﻿using System;
using System.Collections.Generic;

namespace Laboratorio5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Conta> contas = new List<Conta>();

            ContaCorrente cc1 = new ContaCorrente("Joana", 200m);
            ContaCorrente cc2 = new ContaCorrente("Jéssica", 0);
            ContaCorrente cc3 = new ContaCorrente("Jamile", 1000m);
            ContaPoupanca cp1 = new ContaPoupanca(0.06m, "04/11/2018", "Maria");
            ContaPoupanca cp2 = new ContaPoupanca(0.1m, "10/02/2017", "Mônica");
            ContaPoupanca cp3 = new ContaPoupanca(0.05m, "11/04/2019", "Maitê");

            contas.Add(cc1);
            contas.Add(cc2);
            contas.Add(cc3);
            contas.Add(cp1);
            contas.Add(cp2);
            contas.Add(cp3);

            contas.ForEach( c => c.Depositar(100m));

            cc1.Sacar(120m);
            
            Console.WriteLine($"data: {cp1.DataAniversario.Day}/{cp1.DataAniversario.Month}");
            Console.WriteLine($"data: {cp2.DataAniversario.Day}/{cp2.DataAniversario.Month}");
            Console.WriteLine($"data: {cp3.DataAniversario.Day}/{cp3.DataAniversario.Month}");
            
            cp1.AdicionarRendimento();
            cp2.AdicionarRendimento();
            cp3.AdicionarRendimento();

            contas.ForEach( c => Console.WriteLine( $"{c.Id} - Saldo: {c.Saldo:C}" ));


        }
    }
}
